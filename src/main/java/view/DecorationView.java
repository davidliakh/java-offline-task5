package view;
import model.*;
import java.util.*;
public class DecorationView {

    public void findMenu() {
        System.out.println("Welcome Find Menu\n");
        System.out.println("By which parameter do yo wanna find ?\n");
        System.out.println("1)By size\n2)By type\n3)By name\n4)exit " +
                "find menu");
    }

    public void sortMenu() {
        System.out.println("Welcome to Sort Menu\n");
        System.out.println("By which parameter do yo wanna sort the holidays?\n");
        System.out.println("1)By price\n2)By weight\n 3)exit");
    }

    public void printMenu() {
        System.out.println("Welcome\n");
        System.out.println("Choose what you want: \n");
        System.out.println(
                "1)Open find menu\n2)Print all decorations\n3)Open sort menu\n" +
                        "4)Delete by name\n5)exit");
    }

    public void printDecoration(List<Decoration> decorations) {
        for (Decoration decoration : decorations ) {
            System.out.println(decoration.toString());
        }
    }
}

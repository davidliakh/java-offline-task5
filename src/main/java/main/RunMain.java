package main;
import controller.*;
import model.*;
import model.Store.ChristmasStore;
import view.*;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class RunMain {
    private static final int OPEN_FIND_MENU = 1;
    private static final int PRINT_DECORATIONS = 2;
    private static final int SORT_MENU = 3;
    private static final int DELETE_BY_NAME = 4;
    private static final int FIND_BY_SIZE = 2;
    private static final int FIND_BY_TYPE = 1;
    private static final int FIND_BY_NAME = 3;
    private static final int EXIT_FROM_FIND_MENU = 4;
    private static final int EXIT_FROM_SORT_MENU = 3;
    private static final int SORT_BY_WEIGHT = 2;
    private static final int SORT_BY_PRICE = 1;

    private List<Decoration> model;
    private DecorationView view;
    private Controller controller;

    public RunMain() {
        model = new ChristmasStore().getDecoration();
        view = new DecorationView();
        controller = new Controller(model, view);
    }

    public void run() {
        int input;
        do {
            view.printMenu();
            input = chooser(1,5);
            switch (input){
                case OPEN_FIND_MENU:
                    view.findMenu();
                    readFromFindMenu();
                    break;
                case PRINT_DECORATIONS:
                    view.printDecoration(controller.getModel());
                    System.out.println("enter any int");
                    chooser(0,9);
                    break;
                case SORT_MENU:
                    view.sortMenu();
                    readInputFromSortMenu();
                    break;
                case DELETE_BY_NAME:
                    deleteByName();
                    break;
            }
        }while (input!=5);
    }
    private void readInputFromSortMenu() {
        int input;
        do {
            input = chooser(1, 3);
            switch (input) {
                case SORT_BY_PRICE:
                    controller.sortByPrice();
                    break;
                case SORT_BY_WEIGHT:
                    controller.sortByWeight();
                    break;
            }
        } while (input != EXIT_FROM_SORT_MENU);
    }
    private void readFromFindMenu() {
        Scanner scanner = new Scanner(System.in);
        int input;
        do {
            input = chooser(1, 4);
            switch (input) {
                case FIND_BY_SIZE:
                    System.out.println("Enter size : ");
                    double size = scanner.nextDouble();
                    controller.findBySize(size);
                    break;
                case FIND_BY_TYPE:
                    System.out.println("Enter type : ");
                    String type = scanner.next();
                    controller.findByType(type);
                    break;
                case FIND_BY_NAME:
                    System.out.println("Enter name : \n");
                    String name = scanner.nextLine();
                    controller.findByName(name);
                    break;
            }
        } while (input != EXIT_FROM_FIND_MENU);
    }
    private void deleteByName() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter name : \n");
        String name = scanner.nextLine();
        controller.deleteByName(name);
    }

    private int chooser(int firstLimit, int secondLimit) {
        Scanner scanner = new Scanner(System.in);
        int choise = -1;
        while (choise < firstLimit || choise > secondLimit) {
            try {
                System.out.println("Your choice : ");
                choise = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Input number");
                scanner.next();
                continue;
            }
        }
        return choise;
    }
}

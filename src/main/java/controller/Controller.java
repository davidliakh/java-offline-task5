package controller;

import controller.comparator.*;
import model.*;
import model.Store.NewYearStore;
import view.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Controller {
    private List<Decoration> model;
    private DecorationView view;

    public List<Decoration> getModel() {
        return model;
    }

    public DecorationView getView() {
        return view;
    }

    public Controller(List<Decoration> model, DecorationView view) {
        this.model = model;
        this.view = view;
    }

    public List<Decoration> findByType(final String type) {
        ArrayList<Decoration> decorations = new ArrayList<Decoration>();
        for (Decoration decoration : model) {
            if (decoration.getType().equals(type)) {
                decorations.add(decoration);
            }
        }
        view.printDecoration(decorations);
        return decorations;
    }

    public List<Decoration> findBySize(double size) {
        ArrayList<Decoration> decorations = new ArrayList<Decoration>();
        for (Decoration decoration : model) {
            if (decoration.getSize() == size) {
                decorations.add(decoration);
            }
            view.printDecoration(decorations);
        }
        return decorations;
    }

    public Decoration findByName(String name) {
        for (Decoration decoration : model) {
            if (decoration.getName().equals(name)) return decoration;
        }
        return null;
    }

    public void updateView() {
        view.printDecoration(model);
    }

    public void sortByPrice() {
        Collections.sort(model, new PriceComparator());
        updateView();
    }

    public void sortByType() {
    new NewYearStore().getDecoration();
        updateView();
    }

    public void sortByWeight() {
        Collections.sort(model, new WeightComparator());
        updateView();
    }

    public void deleteByName(String name) {
        Decoration decoration = findByName(name);
        if (decoration != null) {
            model.remove(decoration);
            updateView();
        }
    }
}

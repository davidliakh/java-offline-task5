package controller.comparator;

import model.Decoration;

import java.util.Comparator;

public class PriceComparator implements Comparator<Decoration> {
    public int compare(Decoration o1, Decoration o2) {
        return (int)(o1.getPrice() - o2.getPrice());
    }
}

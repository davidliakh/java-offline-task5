package controller.comparator;

import model.Decoration;

import java.util.Comparator;

public class WeightComparator implements Comparator<Decoration> {
    public int compare(Decoration o1, Decoration o2) {
        return (int)(o1.getWeight() - o2.getWeight());
    }
}

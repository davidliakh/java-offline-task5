package model;

public class OutdoorDecoration extends Decoration {
    public OutdoorDecoration(double price, String name, String type, double size, double weight) {
        super(price, name, type, size, weight);
    }
}

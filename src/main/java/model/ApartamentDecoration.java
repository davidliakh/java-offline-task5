package model;

public class ApartamentDecoration extends Decoration {
    public ApartamentDecoration(double price, String name, String type, double size, double weight) {
        super(price, name, type, size, weight);
    }
}

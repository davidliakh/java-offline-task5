package model;

public class Decoration {
    private double price;
    private String name;
    private String type;
    private double size;
    private double weight;

    public Decoration(double price, String name, String type, double size, double weight) {
        this.price = price;
        this.name = name;
        this.type = type;
        this.size = size;
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Name: " + name + "\n" + "Price:" + price + "\n" + "Type:" + type+ "Size:"+ size+"\n"+"Weight" +weight;
    }
}

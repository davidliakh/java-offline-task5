package model.Store;

import model.*;

import java.util.ArrayList;
import java.util.List;

public final class ChristmasStore implements Store{
    private static ChristmasStore instance;
    private List<Decoration> decorations = new ArrayList<Decoration>();

    public ChristmasStore(){
    }
    {
        decorations.add(new ApartamentDecoration(12.4,"d1b","Apartament",12,13));
        decorations.add(new HouseDecoration(13.5,"ff","House",14,15));
        decorations.add(new TreeDecoration(22,"fgfd","Tree",54,345));
        decorations.add(new TreeDecoration(224,"fdfgerd","Tree",34,3.55));
        decorations.add(new OutdoorDecoration(2345,"out","Outdoor",74,14.8));
    }
    public static ChristmasStore getInstance() {
        return instance;
    }

    public List<Decoration> getDecoration(){
        return decorations;
    }
    @Override
    public String toString(){
        return "ChristmasStore decorations" + decorations;
    }
}

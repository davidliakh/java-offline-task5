package model.Store;
import model.*;

import java.util.ArrayList;
import java.util.List;
public class NewYearStore implements Store {
    private static NewYearStore instance;
    private List<Decoration> decorations = new ArrayList<Decoration>();

    public NewYearStore(){
    }
    {
        decorations.add(new ApartamentDecoration(12.7,"hnh","Apartament",12.7,13.8));
        decorations.add(new HouseDecoration(143.5,"ff23","House",14.45,15.65));
        decorations.add(new TreeDecoration(234,"beautiful","Tree",54.65,3443));
        decorations.add(new TreeDecoration(224,"truegame","Tree",345.2,34.55));
        decorations.add(new OutdoorDecoration(25,"outdec","Outdoor",74.6,134.8));
    }
    public static NewYearStore getInstance() {
        return instance;
    }

    public List<Decoration> getDecoration(){
        return decorations;
    }
    @Override
    public String toString(){
        return "NewYearStore decorations" + decorations;
    }

}

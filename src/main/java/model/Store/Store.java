package model.Store;

import model.Decoration;

import java.util.List;

public interface Store {
    List<Decoration> getDecoration();
}

package model;

public class HouseDecoration extends Decoration {
    public HouseDecoration(double price, String name, String type, double size, double weight) {
        super(price, name, type, size, weight);
    }
}
